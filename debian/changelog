golang-github-docker-spdystream (0.2.0-1+apertis1) apertis; urgency=medium

  * Switch component from development to target to comply with Apertis
    license policy.
  * Add debian/apertis/copyright.whitelist
  * Drop debian/apertis/gitlab-ci.yml

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Wed, 22 Jan 2025 13:50:16 +0100

golang-github-docker-spdystream (0.2.0-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 17:22:21 +0000

golang-github-docker-spdystream (0.2.0-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.5.0, no changes needed.

  [ Shengjing Zhu ]
  * New upstream version 0.2.0
  * Exclude ws package.
    No longer used by any other package. Without this, we can
    remove golang-github-gorilla-websocket-dev from Depends
  * Transition to github.com/moby/spdystream
    + Install file in /usr/share/gocode/src/github.com/moby/spdystream
    + Symlink to docker/spdystream
    + Provide golang-github-moby-spdystream-dev package
  * Update Section to golang
  * Bump debhelper-compat to 13
  * Update Standards-Version to 4.5.1 (no changes)
  * Add Multi-Arch hint
  * Add Rules-Requires-Root

 -- Shengjing Zhu <zhsj@debian.org>  Sat, 30 Jan 2021 23:24:49 +0800

golang-github-docker-spdystream (0.0~git20181023.6480d4a-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Anthony Fok ]
  * New upstream version 0.0~git20181023.6480d4a
  * Apply "cme fix dpkg" fixes to debian/control and debian/copyright
    - Bump Standards-Version: '3.9.6' → '4.4.1'
    - Change Priority: 'extra' → 'optional'
    - Add Testsuite: autopkgtest-pkg-go
    - Change Build-Depends: 'debhelper (>= 9)' → 'debhelper-compat (= 12)'
    - Use secure https protocol in Format URL in debian/copyright
  * Update Maintainer email address to team+pkg-go@tracker.debian.org
  * Update dependencies: Change from golang-go and golang-websocket-dev
    to golang-any and golang-github-gorilla-websocket-dev
  * debian/gbp.conf: Disable overlay and export-dir and set default-branch
    to debian/sid
  * Update debian/watch to version=4 and to track git repository directly
  * Simplify debian/rules
  * Add myself to the list of Uploaders

 -- Anthony Fok <foka@debian.org>  Mon, 28 Oct 2019 11:54:22 -0600

golang-github-docker-spdystream (0.0~git20151103.0.4d80814-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 11:51:38 +0000

golang-github-docker-spdystream (0.0~git20151103.0.4d80814-2) unstable; urgency=medium

  [ Paul Tagliamonte ]
  * Team upload.
  * Use a secure transport for the Vcs-Git and Vcs-Browser URL
  * Remove Built-Using from arch:all -dev package

  [ Michael Stapelberg ]
  * Set XS-Go-Import-Path

 -- Michael Stapelberg <stapelberg@debian.org>  Fri, 09 Feb 2018 09:14:18 +0100

golang-github-docker-spdystream (0.0~git20151103.0.4d80814-1) unstable; urgency=medium

  * Initial release (Closes: #798229).

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 03 Nov 2015 11:15:33 +1100
